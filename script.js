const showMenu = ( toggleId, navId ) => {
    const toggle = document.getElementById( toggleId ),
        nav = document.getElementById( navId )
    if ( toggle && nav ) {
        toggle.addEventListener( 'click', () => {
            nav.classList.toggle( 'show-menu' )
        } )
    }
}
showMenu( 'nav-toggle', 'nav-menu' )

function hello( item ) {
    window.alert( "Hallo ini adalah " + item );
}